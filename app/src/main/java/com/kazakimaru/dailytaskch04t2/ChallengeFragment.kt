package com.kazakimaru.dailytaskch04t2

import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.kazakimaru.dailytaskch04t2.databinding.FragmentChallengeBinding


class ChallengeFragment : Fragment() {

    private var _binding: FragmentChallengeBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentChallengeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showAlertDialogStandard()
        showAlertDialogWithAction()
        showtAlertDialogCustom()
        showDialogFragment()
    }

    private fun showAlertDialogStandard() {
        binding.btnAlertdialogstandar.setOnClickListener {
            val dialog = AlertDialog.Builder(requireContext())
            dialog.setTitle(R.string.title)
            dialog.setMessage(R.string.description)
            dialog.setCancelable(true)
            dialog.show()
        }
    }

    private fun showAlertDialogWithAction() {
        binding.btnAlertdialogaction.setOnClickListener {
            val dialog = AlertDialog.Builder(requireContext())
            dialog.setTitle(R.string.title)
            dialog.setMessage(R.string.description)
            dialog.setCancelable(true)
            dialog.setPositiveButton("OK") { _, _ ->
                Toast.makeText(requireContext(), "Berhasil", Toast.LENGTH_SHORT).show()
            }
            dialog.setNegativeButton("Cancel") { dialogInterface, _ ->
                dialogInterface.dismiss()
            }
            dialog.show()
        }
    }

    private fun showtAlertDialogCustom() {
        // Menghubungkan layout
        val customLayout = LayoutInflater.from(requireContext()).inflate(R.layout.dialog_custom_layout, null, false)

        // Mmebuat builder AlertDialog
        val dialogBuilder = AlertDialog.Builder(requireContext())

        // Memanggil textview dan button dari custom layout
        val txtTitle = customLayout.findViewById<TextView>(R.id.title_custom)
        val txtDescription = customLayout.findViewById<TextView>(R.id.description_custom)
        val btnDismiss = customLayout.findViewById<Button>(R.id.btn_custom)

        // Mengubah text dari textview dan button
        txtTitle.text = getString(R.string.title)
        txtDescription.text = getString(R.string.description)
        btnDismiss.text = getString(R.string.btn_dismiss)

        // Mengubah layout AlertDialog menggunakan custom layout
        dialogBuilder.setView(customLayout)

        // Membuat AlertDialog baru dari builder yang sudah di custom layout
        val dialog = dialogBuilder.create()

        // Action pada Button
        btnDismiss.setOnClickListener {
            dialog.dismiss()
        }

        // Action show AlertDialog Custom Layout
        binding.btnAlertdialogcustom.setOnClickListener {
            dialog.show()
        }
    }

    private fun showDialogFragment() {
        binding.btnDialogfragment.setOnClickListener {
            val dialogFragment = MyDialogFragment()
            dialogFragment.show(requireActivity().supportFragmentManager, null)
        }
    }

}