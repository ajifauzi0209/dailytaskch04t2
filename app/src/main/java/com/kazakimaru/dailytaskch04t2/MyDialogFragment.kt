package com.kazakimaru.dailytaskch04t2

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import com.kazakimaru.dailytaskch04t2.databinding.MyDialogFragmentBinding

class MyDialogFragment : DialogFragment() {

    private var _binding: MyDialogFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = MyDialogFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            titleDialogFragment.text = getString(R.string.title)
            descriptionDialogFragment.text = getString(R.string.description)
            btnDialogfragment.text = getString(R.string.btn_dismiss)
        }

        binding.btnDialogfragment.setOnClickListener {
            dialog?.dismiss()
            Toast.makeText(requireContext(), "Berhasil Dismiss", Toast.LENGTH_SHORT).show()
        }
    }
}